﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This script  disables a gameobject after a given time.
/// </summary>
public class GameObjectDisabler : MonoBehaviour
{
    [SerializeField]
    float lifeTime;

    private float timer;
    // Start is called before the first frame update
    void OnEnable()
    {
        timer = lifeTime;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
